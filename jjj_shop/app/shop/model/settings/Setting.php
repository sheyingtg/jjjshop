<?php

namespace app\shop\model\settings;

use think\facade\Cache;
use app\common\model\settings\Setting as SettingModel;
use app\common\enum\settings\SettingEnum;

class Setting extends SettingModel
{
    /**
     * 更新系统设置
     */
    public function edit($key, $values)
    {
        $model = self::detail($key) ?: $this;
        // 删除系统设置缓存
        Cache::delete('setting_' . self::$app_id);
        return $model->save([
                'key' => $key,
                'describe' => SettingEnum::data()[$key]['describe'],
                'values' => $values,
                'app_id' => self::$app_id,
            ]) !== false;
    }

}
